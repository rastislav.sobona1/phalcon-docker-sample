<?php

/**
 * Class BitcoinRateEntity
 */
class BitcoinRateEntity
{
    /** @var string - currency code */
    protected $code;

    /** @var string - currency symbol */
    protected $symbol;

    /** @var float - rate */
    protected $rate;

    /** @var string - description */
    protected $description;

    /** @var float - rate float */
    protected $rate_float;

    /**
     * Get currency code
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * Set currency code
     *
     * @param string $code
     * @return BitcoinRateEntity
     */
    public function setCode(string $code): BitcoinRateEntity
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Get currency symbol
     *
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->symbol;
    }

    /**
     * Set currency symbol
     *
     * @param string $symbol
     * @return BitcoinRateEntity
     */
    public function setSymbol(string $symbol): BitcoinRateEntity
    {
        $this->symbol = $symbol;
        return $this;
    }

    /**
     * Get rate
     *
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * Set rate
     *
     * @param float $rate
     * @return BitcoinRateEntity
     */
    public function setRate(float $rate): BitcoinRateEntity
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return BitcoinRateEntity
     */
    public function setDescription(string $description): BitcoinRateEntity
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get rate float
     *
     * @return float
     */
    public function getRateFloat(): float
    {
        return $this->rate_float;
    }

    /**
     * Set rate float
     *
     * @param float $rate_float
     * @return BitcoinRateEntity
     */
    public function setRateFloat(float $rate_float): BitcoinRateEntity
    {
        $this->rate_float = $rate_float;
        return $this;
    }
}