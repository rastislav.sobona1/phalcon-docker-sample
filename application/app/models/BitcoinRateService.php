<?php

/**
 * Class BitcoinRateService
 */
class BitcoinRateService
{
    /** @var \GuzzleHttp\Client */
    protected $httpClient;

    /** @var string */
    protected $url;

    /**
     * BitcoinRateService constructor.
     * @param $httpClient
     * @param $url
     */
    public function __construct($httpClient, $url)
    {
        $this->httpClient = $httpClient;
        $this->url = $url;
    }

    /**
     * Get bitcoin rates data
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getRatesData()
    {
        $response = $this->httpClient->get($this->url);

        $data = [];
        if($response->getStatusCode() === 200) {
            $data = json_decode($response->getBody()->getContents(), true);
        }

        return \Phalcon\Helper\Arr::get($data, 'bpi', []);
    }
}