<?php
declare(strict_types=1);

/**
 * Class IndexController
 */
class IndexController extends ControllerBase
{

    /**
     * Index action
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function indexAction()
    {
        /** @var BitcoinRateService $bitcoinRateService */
        $bitcoinRateService = $this->di->get('bitcoinRateService');
        $bitcoinRateArr = $bitcoinRateService->getRatesData();

        /** @var \Symfony\Component\Serializer\Serializer $serializer */
        $serializer = $this->di->get('serializer');

        $arrBitcoinRateEntity = [];
        foreach ($bitcoinRateArr as $currencyBitcoinRateData) {
            $arrBitcoinRateEntity [] = $serializer->denormalize($currencyBitcoinRateData, BitcoinRateEntity::class);
        }

        $this->view->setVar('arrBitcoinRateEntity', $arrBitcoinRateEntity);
    }

}

