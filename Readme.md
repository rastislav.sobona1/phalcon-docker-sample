### Phalcon project with Docker - sample


Clone project

    git clone https://gitlab.com/rastislav.sobona1/phalcon-docker-sample.git

Go to project folder

    cd phalcon-docker-sample

Build project and run
    
    make
    
Project url

    http://localhost:8910/